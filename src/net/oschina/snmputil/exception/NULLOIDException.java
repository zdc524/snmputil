package net.oschina.snmputil.exception;

/**
 * 
 * @author zdc 无此OID的异常
 */
public class NULLOIDException extends Exception {

	private static final long serialVersionUID = 1L;

	public NULLOIDException() {
		super();

	}

	public NULLOIDException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public NULLOIDException(String message, Throwable cause) {
		super(message, cause);

	}

	public NULLOIDException(String message) {
		super(message);

	}

	public NULLOIDException(Throwable cause) {
		super(cause);

	}

}
