package net.oschina.snmputil.snmp;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map.Entry;

import net.oschina.snmputil.domain.Triple;
import net.oschina.snmputil.domain.ValueType;
import net.oschina.snmputil.exception.NULLOIDException;

import org.snmp4j.CommunityTarget;

/**
 * 
 * @author zdc
 *
 */
public interface SnmpService {
    /**
   * 
   */
	void setUseTcp();

	void setRetries(int retries);

	void setTimeout(long timeout);

	/**
	 * 
	 * @param version
	 * @param writeCommity
	 * @param readCommity
	 */
	void setCommunity(int version, String writeCommity, String readCommity);

	/**
	 * 
	 * @return
	 */
	CommunityTarget getTarget();

	/*****
	 * 
	 * 
	 * @param oid
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	Entry<String, String> getBulk(String... oids) throws IOException;

	/**
	 * 
	 * 
	 * @param oid
	 * @return Entry<oid, value>
	 * @throws Exception
	 */
	Entry<String, String> getWithIndex(String oid) throws Exception;

	/**
	 * 
	 * @param oid
	 * @return value
	 * @throws Exception
	 */
	String get(String oid) throws Exception;
    /**
     * 
     * 批量获取 OID
     * /
	Entry<String, String>[]  getList (String [] index) throws Exception;

	/**
	 * 
	 * 
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	Entry<String, String> getNext(String oid) throws Exception;

	/**
	 * 
	 * 
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	Entry<String, String>[] walk(String oid) throws Exception;

	/**
	 * 
	 * @param oid
	 * @param index
	 * @return
	 * @throws Exception
	 */
	Entry<String, String>[] walk(String oid, String index) throws Exception;

	/**
	 * 
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	Entry<String, String>[] newwalk(String oid) throws Exception;

	/**
	 * 
	 * 
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	Entry<String, String>[] bulkWalk(String... oid) throws Exception;

	/**
	 * 
	 * @param i
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	Entry<String, String>[] bulkWalk(int i, String... oid) throws Exception;

	/**
	 * 
	 * @param oid
	 * @param value
	 * @param vt
	 * @throws Exception
	 */
	void set(String oid, Object value, ValueType vt) throws Exception;

	/**
	 * 
	 * @param binds
	 * @return
	 * @throws NULLOIDException
	 * @throws IOException
	 * @throws Exception
	 */
	int set(List<Triple<String, Object, ValueType>> binds) throws IOException,
			NULLOIDException;

	/**
	 * 
	 * @param ip
	 * @param port
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	void openSession(String ip, int port) throws UnknownHostException,
			IOException;

	/**
	 * 
	 * @param ip
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	void openSession(String ip) throws UnknownHostException, IOException;

	/**
	 * 
	 * @return
	 */
	boolean closeSession();
}