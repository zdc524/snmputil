package net.oschina.snmputil.tlv.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * 
 */
public class TlvUtils {

	/**
	 * 
	 * 
	 * @param hexString
	 * @return
	 */
	public static List<Tlv> builderTlvList(String hexString) {
		List<Tlv> tlvs = new ArrayList<Tlv>();

		int position = 0;
		while (position != StringUtils.length(hexString)) {
			String _hexTag = getTag(hexString, position);
			position += _hexTag.length();

			LPositon l_position = getLengthAndPosition(hexString, position);
			int _vl = l_position.get_vL();

			position = l_position.get_position();

			String _value = StringUtils.substring(hexString, position, position
					+ _vl * 2);

			position = position + _value.length();

			tlvs.add(new Tlv(_hexTag, _vl, _value));
		}
		return tlvs;
	}

	/**
	 * 
	 * 
	 * @param hexString
	 * @return
	 */
	public static Map<String, Tlv> builderTlvMap(String hexString) {

		Map<String, Tlv> tlvs = new HashMap<String, Tlv>();

		int position = 0;
		System.out.println(hexString.length());
		while (position != hexString.length()) {
			String _hexTag = getTag(hexString, position);
			System.out.println(_hexTag);
			position += _hexTag.length();

			System.out.println(position);
			LPositon l_position = getLengthAndPosition(hexString, position);

			int _vl = l_position.get_vL();
			position = l_position.get_position();
			String _value = hexString.substring(position, position + _vl * 2);
			position = position + _value.length();

			tlvs.put(_hexTag, new Tlv(_hexTag, _vl, _value));
		}
		return tlvs;
	}

	/**
	 * 
	 * 
	 * @param hexString
	 * @param position
	 * @return
	 */
	private static LPositon getLengthAndPosition(String hexString, int position) {
		String firstByteString = hexString.substring(position, position + 2);
		int i = Integer.parseInt(firstByteString, 16);
		String hexLength = "";

		if (((i >>> 7) & 1) == 0) {
			hexLength = hexString.substring(position, position + 2);
			position = position + 2;
		} else {
		
			int _L_Len = i & 127;
			position = position + 2;
			hexLength = hexString.substring(position, position + _L_Len * 2);
		
			position = position + _L_Len * 2;
		}
		return new LPositon(Integer.parseInt(hexLength, 16), position);

	}

	/**
	 *
	 * 
	 * @param hexString
	 * @param position
	 * @return
	 */
	private static String getTag(String hexString, int position) {
		String firstByte = StringUtils.substring(hexString, position,
				position + 2);
		// System.out.println(firstByte);
		int i = Integer.parseInt(firstByte, 16);
		if ((i & 0x1f) == 0x1f) {
			return hexString.substring(position, position + 4);

		} else {
			return hexString.substring(position, position + 2);
		}
	}

	public static void main(String[] args) {
		DecimalFormat df1 = new java.text.DecimalFormat("#.###");
		// 2B060104018188010103010D00
		// 060D2B060104018188010103010D0002020026
		// 060d2b060104018188010103010d00020200
		// 060e2b0601040181880101090301020102022601
		String string = "-14";
		Double d = Double.parseDouble(string) / 100;
		System.out.println(df1.format(d));
		// Map<String, Tlv> map1 =
		// TlvUtils.builderTlvMap("060E2B0601040181880101090501040202020000");
		// //Iterator iter = map1.entrySet().iterator();
		// while (iter.hasNext()) {
		// Map.Entry entry = (Map.Entry) iter.next();
		// Object key = entry.getKey();
		// // System.out.println(key);
		// Tlv val = (Tlv) entry.getValue();
		// System.out.println("tag:" + val.getTag());
		// System.out.println("length:" + val.getLength());
		// System.out.println("value:" + val.getValue());
		//
		// }
	}
}
