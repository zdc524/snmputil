#snmputil
![许可](https://img.shields.io/badge/license-GPL-green.svg)
初步添加的是 tlv 的解析工具,添加IP地址转换工具（IPV4）
需使用[java commons](http://commons.apache.org/proper/commons-lang/)
##异步snmp
##bulk snmp
```java
	private Entry<String, String>[] walkImplBulk(String rootId,

			int maxRePetitions, String... oids) throws IOException {

		target.setCommunity(new OctetString(readCommunity));

		PDU request = new PDU();

		request.setType(PDU.GETBULK);



		for (String oid : oids) {

			request.add(new VariableBinding(new OID(oid)));

		}

		request.setMaxRepetitions(maxRePetitions);

		request.setNonRepeaters(0);

		// request.setErrorStatus(0);


		// request.setErrorIndex(0);


		ResponseEvent rspEvt = curSession.send(request, target);

		// System.out.println("xxxxxxxxxxx");


		PDU response = rspEvt.getResponse();

		// System.out.println(response);


		List<Entry<String, String>> lists = new LinkedList<Entry<String, String>>();

		if (null != response && response.getErrorIndex() == PDU.noError

				&& response.getErrorStatus() == PDU.noError) {



			Vector<?> vector = response.getVariableBindings();

			// System.out.println(vector.size());


			Entry<String, String>[] val = new Map.Entry[vector.size()];



			for (Object variable : vector) {

				VariableBinding binding = (VariableBinding) variable;

				// System.out.println(binding.getOid().toString());


				// System.out.println(binding.getVariable().toString());


				lists.add(new AbstractMap.SimpleImmutableEntry<String, String>(

						binding.getOid().toString(), binding.getVariable()

								.toString()));

			}

			return lists.toArray(val);



		}

		return null;

	}
```